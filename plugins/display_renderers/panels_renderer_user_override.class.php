<?php

/**
 * Renderer class for the User Override behavior.
 */
class panels_renderer_user_override extends panels_renderer_standard {

  function init($plugin, &$display) {
    parent::init($plugin, $display);
  }

  function prepare($external_settings = NULL) {
    global $user;
    if (!user_is_anonymous()) {
      // Put the panes in the user defined regions.
      $override = db_select('panels_override', 'po')
        ->fields('po', array('data'))
        ->condition('did', $this->display->did)
        ->condition('type', 'user')
        ->condition('external_id', $user->uid)
        ->execute()
        ->fetch();
      if ($override) {
        $data = unserialize($override->data);
        foreach ($data as $key => $value) {
          if (!empty($this->display->{$key})) {
            $this->display->{$key . '_orginal'} = $this->display->{$key};
          }
          $this->display->{$key} = $value;
        }
      }
    }
    // Save to_render array for later use.
    $to_render = array();
    foreach($this->display->panels as $region => $panes) {
      if ($region != '_contentstock_') {
        $to_render = array_merge($to_render, $panes);
      }
    }

    // remove region _contentstock_ and its content
    if (isset($this->display->panels['_contentstock_'])) {
      $remove = $this->display->panels['_contentstock_'];
      unset($this->display->panels['_contentstock_']);
      foreach($remove as $cid) {
        if (isset($this->display->content[$cid]) && !in_array($cid, $to_render)) {
          unset($this->display->content[$cid]);
        }
      }
    }
    // Search missing content.
    if (!empty($this->display->panels_orginal)) {
      $missing = array_diff(
        array_keys($this->display->content),
        $to_render
      );
      if (count($missing)) {
        foreach ($this->display->panels_orginal as $region => $panes) {
          if ($region == '_contentstock_') {
            continue;
          }
          $found = array_intersect($panes, $missing);
          // remove found from missing
          $missing = array_diff($missing, $found);
          foreach($found as $position => $cid) {
            if (!isset($this->display->panels[$region])) {
              $this->display->panels[$region] = array();
            }
            // try to slice it in the right position.
            $this->display->panels[$region] = array_merge(
              array_slice($this->display->panels[$region], 0, $position),
              array($position => $cid),
              array_slice($this->display->panels[$region], $position));
          }
        }
        // Still got missing? They were in _contentstock_
        foreach($missing as $cid) {
          if (isset($this->display->content[$cid])) {
            unset($this->display->content[$cid]);
          }
        }
      }
    }
    parent::prepare($external_settings);
  }
}


