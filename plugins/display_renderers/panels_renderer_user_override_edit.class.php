<?php

/**
 * Renderer class for the User Override Edit behavior.
 */
class panels_renderer_user_override_edit extends panels_renderer_editor {


  var $edit_state_default = FALSE;

  function init($plugin, &$display) {
    parent::init($plugin, $display);
    // Add our stock region
    $this->plugins['layout']['regions']['_contentstock_'] = t('Content Stock');
  }

  function prepare($external_settings = NULL) {
    global $user;
    if (!$this->edit_state_default && !user_is_anonymous()) {
      // Put the panes in the user defined regions.
      $override = db_select('panels_override', 'po')
        ->fields('po', array('data'))
        ->condition('did', $this->display->did)
        ->condition('type', 'user')
        ->condition('external_id', $user->uid)
        ->execute()
        ->fetch();
      if ($override) {
        $data = unserialize($override->data);
        foreach ($data as $key => $value) {
          if (!empty($this->display->{$key})) {
            $this->display->{$key . '_orginal'} = $this->display->{$key};
          }
          $this->display->{$key} = $value;
        }
        // Search missing content.
        if (!empty($this->display->panels_orginal)) {
          $missing = array_keys($this->display->content);
          foreach($this->display->panels as $to_render) {
            $missing = array_diff($missing, $to_render);
          }
          if (count($missing)) {
            foreach ($this->display->panels_orginal as $region => $panes) {
              $found = array_intersect($panes, $missing);
              // remove found from missing
              $missing = array_diff($missing, $found);
              foreach($found as $position => $cid) {
                if (!isset($this->display->panels[$region])) {
                  $this->display->panels[$region] = array();
                }
                // try to slice it in the right position.
                $this->display->panels[$region] = array_merge(
                  array_slice($this->display->panels[$region], 0, $position),
                  array($position => $cid),
                  array_slice($this->display->panels[$region], $position));
              }
            }
            // push the rest in stock;
            $this->display->panels['_contentstock_'] = array_merge(
              $this->display->panels['_contentstock_'],
              $missing
            );
          }
        }
      }
    }
    parent::prepare($external_settings);
  }

  /**
   * We need to render in our stock region.
   */
  function render_layout() {
    if (empty($this->prep_run)) {
      $this->prepare();
    }
    $this->render_panes();
    $this->render_regions();

    if ($this->admin && !empty($this->plugins['layout']['admin theme'])) {
      $theme = $this->plugins['layout']['admin theme'];
    }
    else {
      $theme = $this->plugins['layout']['theme'];
    }
    $this->rendered['layout'] = $this->rendered['regions']['_contentstock_'];
    $this->rendered['layout'] .= theme($theme, array('css_id' => check_plain($this->display->css_id), 'content' => $this->rendered['regions'], 'settings' => $this->display->layout_settings, 'display' => $this->display, 'layout' => $this->plugins['layout'], 'renderer' => $this));

    return $this->prefix . $this->rendered['layout'] . $this->suffix;
  }
}


