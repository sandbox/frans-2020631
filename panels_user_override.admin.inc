<?php

/**
 * Menu callback
 * Render a table with all editable displays.
 */
function panels_user_override_displays($account) {
  ctools_include('export');
  ctools_include('context-task-handler');
  ctools_include('plugins');
  $header = array(
    t('Name'),
    t('Operations'),
  );

  // Fetch existing.
  $existing = db_select('panels_override', 'po')
    ->fields('po', array('did', 'did'))
    ->condition('type', 'user')
    ->condition('external_id', $account->uid)
    ->execute()
    ->fetchAllKeyed();

  $rows = array();
  $results = db_select('page_manager_handlers', 'h')
    ->fields('h')
    ->execute();
  foreach($results as $result) {
    $handler = ctools_export_unpack_object('page_manager_handlers', $result);
    // TODO: access control!.
    if ($handler->conf['pipeline'] == 'panels_user_override') {
      $row = array();
      $row[] = check_plain($handler->conf['title']);
      $op = l(t('edit'), 'user/' . $account->uid . '/display-override/edit/' . $handler->did, array('query' => drupal_get_destination()));
      if (in_array($handler->did, $existing)) {
        $op .= ' '  . l(t('reset'), 'user/' . $account->uid . '/display-override/reset/' . $handler->did, array('query' => drupal_get_destination()));
      }
      $row[] = $op;
      $rows[] = $row;
    }
  }
  return array(
    '#theme' => 'table',
    '#empty' => t('No displays found.'),
    '#rows' => $rows,
    '#header' => $header,
  );
}


/**
 * Form that lets a user override its display.
 */
function panels_user_override_displays_form($form, &$form_state, $account, $did) {
  ctools_include('display-edit', 'panels');
  ctools_include('export');
  ctools_include('plugins', 'panels');



  $handler = db_select('page_manager_handlers', 'h')
    ->fields('h')
    ->condition('did', $did)
    ->execute()
    ->fetch();
  $handler = ctools_export_unpack_object('page_manager_handlers', $handler);
  if ($handler->conf['pipeline'] != 'panels_user_override') {
    drupal_not_found();
  }

  $form['#account'] = $account;
  $form['#did'] = $did;
  $form['#handler'] = $handler;

  $form_state['task_name'] = $handler->task . (!empty($handler->subtask)?'-' . $handler->subtask:'');
  $form_state['handler_id'] = $handler->name;
  $form_state['subtask_id'] = $handler->subtask;
  $cache = panels_edit_cache_get('panel_context:' . $form_state['task_name'] . ':' . $form_state['handler_id']);

  $cache->display->no_flexible_editor = TRUE;

  $form['#original'] = $cache->display->panels;

  $form_state['renderer'] = panels_get_renderer_handler('user_override_edit', $cache->display);
  $form_state['renderer']->cache = &$cache;

  $form_state['display'] = &$cache->display;
  $form_state['content_types'] = $cache->content_types;
  $form_state['display_title'] = 0;
  $form_state['no display settings'] = TRUE;
  $form_state['no preview'] = TRUE;
  $form_state['no buttons'] = TRUE;

  $form = panels_edit_display_form($form, $form_state);

  $form['buttons'] = array(
    '#type' => 'actions',
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  // Prepare cancel link.
  if (isset($_GET['destination'])) {
    $options = drupal_parse_url(urldecode($_GET['destination']));
  }
  else {
    $options = array('path' => 'user/' . $account->uid . '/display-override');
  }

  $form['buttons']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => $options['path'],
    '#options' => $options,
  );
  $form['#attached']['css'][] = drupal_get_path('module', 'panels_user_override') . '/panels_user_override.css';

  return $form;
}

/**
 * Submit handler.
 */
function panels_user_override_displays_form_submit(&$form, &$form_state) {
  $panel = &$form_state['values']['panel'];
  foreach($panel['pane'] as $key => &$region) {
    if (strlen($region)) {
      $region = explode(',', $region);
    }
    else {
      unset($panel['pane'][$key]);
    }
  }
  $data = array('panels' => $panel['pane']);
  $save = array(
    'did' => $form['#did'],
    'type' => 'user',
    'external_id' => $form['#account']->uid,
    'data' => $data,
  );
  // Delete before save.
  db_delete('panels_override')
    ->condition('did', $save['did'])
    ->condition('type', $save['type'])
    ->condition('external_id', $save['external_id'])
    ->execute();

  // Calculate the difference in the original and the new array.
  $needs_save = (bool)array_diff_assoc(
    array_map('serialize', $form['#original']),
    array_map('serialize', $data['panels'])
  );
  // Save
  if ($needs_save) {
    drupal_write_record('panels_override', $save);
  }
  drupal_set_message(t('Display %title has been saved.', array('%title' => $form['#handler']->conf['title'])));
}

/**
 * Form that lets a user override its display.
 */
function panels_user_override_displays_reset_form($form, &$form_state, $account, $did) {
  ctools_include('export');
  $handler = db_select('page_manager_handlers', 'h')
    ->fields('h')
    ->condition('did', $did)
    ->execute()
    ->fetch();
  $handler = ctools_export_unpack_object('page_manager_handlers', $handler);
  if ($handler->conf['pipeline'] != 'panels_user_override') {
    drupal_not_found();
  }
  $form['#account'] = $account;
  $form['#did'] = $did;
  $form['#handler'] = $handler;
  $form = confirm_form($form,
    t('Are you sure you want to reset display %title?',
    array('%title' => $handler->conf['title'])),
    'user/' . $account->uid . '/display-override',
    NUll,
    t('Reset')
  );
  return $form;
}

/**
 * Submit handler.
 */
function panels_user_override_displays_reset_form_submit(&$form, &$form_state) {
  $delete = array(
    'did' => $form['#did'],
    'type' => 'user',
    'external_id' => $form['#account']->uid,
  );
  // Delete.
  db_delete('panels_override')
    ->condition('did', $delete['did'])
    ->condition('type', $delete['type'])
    ->condition('external_id', $delete['external_id'])
    ->execute();
  drupal_set_message(t('Display %title has been reset.', array('%title' => $form['#handler']->conf['title'])));
}